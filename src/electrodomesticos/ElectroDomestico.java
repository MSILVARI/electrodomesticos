/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electrodomesticos;

/**
 *
 * @author milton
 */
public abstract class ElectroDomestico implements AlamacenElectro {

    private String nombre, marca, color;
    private int cantidad;
    private double precio;

    public ElectroDomestico(String nombre, String marca, String color, int cantidad, double precio) {
        this.nombre = nombre;
        System.out.println("Alamacena , "
                + "nombre del Electrodomestico : " + this.nombre);
        this.marca = marca;
        System.out.println("Marca ," + this.marca);
        this.color = color;
        System.out.println("Color , "  + this.color);
        this.cantidad = cantidad;
        System.out.println("Cantidad  , "  + this.cantidad);
        this.precio = precio;
        System.out.println("Precio  , " + this.precio);
    }

    public ElectroDomestico() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public abstract void tipoAlmacen();

}
